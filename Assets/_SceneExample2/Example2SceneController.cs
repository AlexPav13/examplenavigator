﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LimeKit.Core;

public class Example2SceneController : SceneController
{
    public void OnClickBack()
    {
        Navigator.Shared.Pop(TransitionAnimation.CrossDissolve);
    }

    public void OnClickPopup()
    {
        Navigator.Shared.PushFrom<Example2SceneController>("Example1", "Example3", PresentationMode.NewOpaqueLevel, TransitionAnimation.CrossDissolve);
    }
}
