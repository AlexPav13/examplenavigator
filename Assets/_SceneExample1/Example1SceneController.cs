﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LimeKit.Core;

public class Example1SceneController : SceneController
{
    public void Start()
    {
        Navigator.Shared.SetRoot<Example1SceneController>(this.gameObject.scene);
    }

    public void OnClickStart()
    {
        Navigator.Shared.Push<Example2SceneController>("Example2", PresentationMode.NewOpaqueLevel, TransitionAnimation.CrossDissolve);
    }
}
