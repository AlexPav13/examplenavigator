using System;
using System.Collections.Generic;

namespace LimeKit
{
    public sealed class MessageBroker
    {
        public readonly static MessageBroker Shared = new MessageBroker();

        readonly Dictionary<Type, List<_Registration>> _registry = new Dictionary<Type, List<_Registration>>();

        public void Publish<T>(T message)
        {
            Type key = typeof(T);
            if (_registry.ContainsKey(key))
            {
                var registrationsCopy = new List<_Registration>(_registry[key]); // copy for thread safety
                foreach (var registration in registrationsCopy)
                    registration.observer(message);
            }
        }

        public void AddObserver<T>(Action<T> observer)
        {
            lock(_registry)
            {
                Type key = typeof(T);
                var registrations = _registry.ContainsKey(key) ? _registry[key] : new List<_Registration>();
                registrations.Add(new _Registration { reference = observer, observer = obj => observer((T) obj) });
                _registry[key] = registrations;
            }
        }

        public void RemoveObserver<T>(Action<T> observer)
        {
            lock(_registry)
            {
                Type key = typeof(T);
                if (_registry.ContainsKey(key))
                {
                    var registrations = _registry[key];
                    registrations.RemoveAll(reg => reg.reference.Equals(observer));
                    _registry[key] = registrations;
                }
            }
        }

        class _Registration
        {
            public object reference;
            public Action<object> observer;
        }
    }
}