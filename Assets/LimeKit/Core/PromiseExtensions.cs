using System;
using System.Threading.Tasks;

namespace LimeKit.Core
{
    public static class PromiseExtensions
    {
        public static Promise<T> ToPromise<T>(this Task<T> task)
        {
            var promise = new Promise<T>();
            task.ContinueWith(t =>
            {
                if (t.Exception != null)
                    promise.Reject(t.Exception);
                else
                    promise.Resolve(t.Result);
            });
            return promise;
        }

        public static Promise ToPromise(this Task task)
        {
            var promise = new Promise();
            task.ContinueWith(t =>
            {
                if (t.Exception != null)
                    promise.Reject(t.Exception);
                else
                    promise.Resolve();
            });
            return promise;
        }

        public static Task<T> ToTask<T>(this Promise<T> promise)
        {
            var tcs = new TaskCompletionSource<T>();
            promise.Completed += (value, error) =>
            {
                if (error != null) tcs.TrySetException(error);
                else tcs.TrySetResult(value);
            };
            return tcs.Task;
        }

        public static Task ToTask(this Promise promise)
        {
            var tcs = new TaskCompletionSource<object>();
            promise.Completed += (error) =>
            {
                if (error != null) tcs.TrySetException(error);
                else tcs.TrySetResult(null);
            };
            return tcs.Task as Task;
        }

        public static Promise<R> Then<T, R>(this Promise<T> promise, Func<T, Promise<R>> onResolved)
        {
            return Done(promise, (res, err) =>
            {
                if (err != null)
                    return Promise<R>.Rejected(err);
                else
                    return onResolved(res);
            });
        }

        public static Promise Then<T>(this Promise<T> promise, Func<T, Promise> onResolved)
        {
            return Done(promise, (res, err) =>
            {
                if (err != null)
                    return Promise.Rejected(err);
                else
                    return onResolved(res);
            });
        }

        public static Promise<R> Then<R>(this Promise promise, Func<Promise<R>> onResolved)
        {
            return Done(promise, (err) =>
            {
                if (err != null)
                    return Promise<R>.Rejected(err);
                else
                    return onResolved();
            });
        }

        public static Promise Then(this Promise promise, Func<Promise> onResolved)
        {
            return Done(promise, (err) =>
            {
                if (err != null)
                    return Promise.Rejected(err);
                else
                    return onResolved();
            });
        }

        public static Promise<T> Then<T>(this Promise<T> promise, Action<T> onResolved)
        {
            return promise.Done((res, err) =>
            {
                if (err == null) onResolved(res);
            });
        }

        public static Promise Then(this Promise promise, Action onResolved)
        {
            return promise.Done((err) =>
            {
                if (err == null) onResolved();
            });
        }

        public static Promise<T> Catch<T>(this Promise<T> promise, Action<Exception> onRejected)
        {
            return promise.Done((res, err) =>
            {
                if (err != null) onRejected(err);
            });
        }

        public static Promise Catch(this Promise promise, Action<Exception> onRejected)
        {
            return promise.Done((err) =>
            {
                if (err != null) onRejected(err);
            });
        }

        public static Promise<R> Done<T, R>(this Promise<T> promise, Func<T, Exception, Promise<R>> onCompleted)
        {
            var returnedPromise = new Promise<R>();
            promise.Completed += (res, err) =>
            {
                var nextPromise = onCompleted(res, err);
                nextPromise.Done((res2, err2) =>
                {
                    if (err2 != null)
                        returnedPromise.Reject(err2);
                    else
                        returnedPromise.Resolve(res2);
                });
                returnedPromise.Canceled += nextPromise.Cancel;
            };
            returnedPromise.Canceled += promise.Cancel;
            return returnedPromise;
        }

        public static Promise Done<T>(this Promise<T> promise, Func<T, Exception, Promise> onCompleted)
        {
            var returnedPromise = new Promise();
            promise.Completed += (res, err) =>
            {
                var nextPromise = onCompleted(res, err);
                nextPromise.Done((err2) =>
                {
                    if (err2 != null)
                        returnedPromise.Reject(err2);
                    else
                        returnedPromise.Resolve();
                });
                returnedPromise.Canceled += nextPromise.Cancel;
            };
            returnedPromise.Canceled += promise.Cancel;
            return returnedPromise;
        }

        public static Promise<R> Done<R>(this Promise promise, Func<Exception, Promise<R>> onCompleted)
        {
            var returnedPromise = new Promise<R>();
            promise.Completed += (err) =>
            {
                var nextPromise = onCompleted(err);
                nextPromise.Done((res2, err2) =>
                {
                    if (err2 != null)
                        returnedPromise.Reject(err2);
                    else
                        returnedPromise.Resolve(res2);
                });
                returnedPromise.Canceled += nextPromise.Cancel;
            };
            returnedPromise.Canceled += promise.Cancel;
            return returnedPromise;
        }

        public static Promise Done(this Promise promise, Func<Exception, Promise> onCompleted)
        {
            var returnedPromise = new Promise();
            promise.Completed += (err) =>
            {
                var nextPromise = onCompleted(err);
                nextPromise.Done((err2) =>
                {
                    if (err2 != null)
                        returnedPromise.Reject(err2);
                    else
                        returnedPromise.Resolve();
                });
                returnedPromise.Canceled += nextPromise.Cancel;
            };
            returnedPromise.Canceled += promise.Cancel;
            return returnedPromise;
        }

        public static Promise<T> Done<T>(this Promise<T> promise, Action<T, Exception> onCompleted)
        {
            if (promise.IsCompleted)
                onCompleted(promise.Value, promise.Error);
            else
                promise.Completed += onCompleted;
            return promise;
        }

        public static Promise Done(this Promise promise, Action<Exception> onCompleted)
        {
            if (promise.IsCompleted)
                onCompleted(promise.Error);
            else
                promise.Completed += onCompleted;
            return promise;
        }

        public static Promise<T> Progress<T>(this Promise<T> promise, Action<float> progressHandler)
        {
            promise.ProgressChanged += progressHandler;
            return promise;
        }

        public static Promise Progress(this Promise promise, Action<float> progressHandler)
        {
            promise.ProgressChanged += progressHandler;
            return promise;
        }
    }
}