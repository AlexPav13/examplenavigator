using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LimeKit.Core
{
    public enum PresentationMode
    {
        ReplacePrevious,
        NewOpaqueLevel,
        NewTransparentLevel
    }

    public interface SceneInfo
    {
        int Index { get; }
        string Name { get; }
        int Level { get; }
        PresentationMode PresentationMode { get; }
        Dictionary<string, object> Data { get; }
        SceneController Controller { get; }
        TransitionAnimation TransitionAnimation { get; set; }
        bool IsLoaded { get; }
    }

    public abstract class PushOperation : CustomYieldInstruction
    {
        public abstract bool ReadyForSceneActivation { get; }
        public abstract bool ReadyForSceneAppearing { get; }
        public abstract bool AllowSceneActivation { get; set; }
        public abstract bool AllowSceneAppearing { get; set; }
        public abstract float LoadProgress { get; }
        public abstract event Action Completed;
    }

    public abstract class PushOperation<TController> : PushOperation where TController : SceneController
    {
        public abstract TController SceneController { get; }
    }

    public abstract class PopOperation : CustomYieldInstruction
    {
        public abstract event Action Completed;
    }

    public abstract class RemoveOperation : CustomYieldInstruction
    {
        public abstract event Action Completed;
    }

    public sealed class Navigator : MonoBehaviour
    {
        public static Navigator Shared
        {
            get
            {
                if (_shared == null || _shared._isDestroyed)
                {
                    var go = new GameObject("Navigator (Shared)");
                    _shared = go.AddComponent<Navigator>();
                    DontDestroyOnLoad(_shared);
                }
                return _shared;
            }
        }
        static Navigator _shared;

        public delegate void NavigationEventHandler(Navigator navigator, SceneInfo from, SceneInfo to);
        public event NavigationEventHandler OnPush;
        public event NavigationEventHandler OnPop;

        public delegate void TransitionEventHandler(Navigator navigator, SceneInfo scene);
        public event TransitionEventHandler OnSceneWillAppear;
        public event TransitionEventHandler OnSceneDidAppear;
        public event TransitionEventHandler OnSceneWillDisappear;
        public event TransitionEventHandler OnSceneDidDisappear;

        public TransitionAnimation DefaultAnimation = TransitionAnimation.NoAnimation;
        public List<TransitionAnimator> FallbackAnimator;
        public Func<SceneInfo, bool> UnloadFilter;

        [SerializeField] bool _setShared = true;

        List<_SceneInfo> _scenes = new List<_SceneInfo>();
        bool _isDestroyed;

        static List<Scene> _loadedScenes = new List<Scene>();

        public int ScenesCount => _scenes.Count;

        public List<SceneInfo> GetAllScenes()
        {
            return _scenes.Cast<SceneInfo>().ToList();
        }

        public SceneInfo GetLastScene()
        {
            return GetScene(-1);
        }

        public SceneInfo GetScene(int atIndex)
        {
            if (atIndex >= 0 && atIndex < _scenes.Count)
                return _scenes[atIndex];
            else if (atIndex < 0 && atIndex >= -_scenes.Count)
                return _scenes[_scenes.Count + atIndex];
            else
                return null;
        }

        public PushOperation<TController> SetRoot<TController>(string sceneName, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var removeAll = _scenes.Count > 0 ? _RemoveCoroutine(_scenes) : null;
            var pushOperation = new _PushOperation<TController>();
            StartCoroutine(_PushFromCoroutine<TController>(null, sceneName, default, PresentationMode.NewOpaqueLevel, animation, initializer, removeAll, pushOperation, asyncLoad));
            return pushOperation;
        }

        public PushOperation<TController> SetRoot<TController>(Scene scene, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var removeAll = _scenes.Count > 0 ? _RemoveCoroutine(_scenes) : null;
            var pushOperation = new _PushOperation<TController>();
            StartCoroutine(_PushFromCoroutine<TController>(null, scene.name, scene, PresentationMode.NewOpaqueLevel, animation, initializer, removeAll, pushOperation, asyncLoad));
            return pushOperation;
        }

        public PushOperation<TController> Push<TController>(string sceneName, PresentationMode mode, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var pushOperation = new _PushOperation<TController>();
            if (_scenes.Count < 1) { _AddSceneInScenes(); }
            StartCoroutine(_PushFromCoroutine<TController>(null, sceneName, default, _scenes.Count > 0 ? mode : PresentationMode.NewOpaqueLevel, animation, initializer, null, pushOperation, asyncLoad));
            return pushOperation;
        }

        public PushOperation<TController> Push<TController>(Scene scene, PresentationMode mode, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var pushOperation = new _PushOperation<TController>();
            if (_scenes.Count < 1) { _AddSceneInScenes(); }
            StartCoroutine(_PushFromCoroutine<TController>(null, scene.name, scene, _scenes.Count > 0 ? mode : PresentationMode.NewOpaqueLevel, animation, initializer, null, pushOperation, asyncLoad));
            return pushOperation;
        }

        void _AddSceneInScenes()
        {
            Scene scene = SceneManager.GetActiveScene();
            var fromScene = new _SceneInfo
            {
                Index = 0,
                Name = scene.name,
                Level = 0,
                PresentationMode = PresentationMode.ReplacePrevious,
                Initializer = null
            };
            _scenes.Add(fromScene);
        }

        public PushOperation<TController> PushFrom<TController>(string sceneNameFrom, string sceneNameTo, PresentationMode mode, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var pushOperation = new _PushOperation<TController>();
            SceneInfo _sceneFrom = _scenes.Find((sceneInfo) => { return sceneInfo.Name == sceneNameFrom; });
            StartCoroutine(_PushFromCoroutine<TController>(_sceneFrom, sceneNameTo, default, _scenes.Count > 0 ? mode : PresentationMode.NewOpaqueLevel, animation, initializer, null, pushOperation, asyncLoad));
            return pushOperation;
        }

        public PushOperation<TController> PushFrom<TController>(Scene sceneFrom, Scene sceneTo, PresentationMode mode, TransitionAnimation animation = null, Action<TController> initializer = null, bool asyncLoad = true) where TController : SceneController
        {
            var pushOperation = new _PushOperation<TController>();
            SceneInfo _sceneFrom = _scenes.Find((sceneInfo) => { return sceneInfo.Name == sceneFrom.name; });
            StartCoroutine(_PushFromCoroutine<TController>(_sceneFrom, sceneTo.name, sceneTo, _scenes.Count > 0 ? mode : PresentationMode.NewOpaqueLevel, animation, initializer, null, pushOperation, asyncLoad));
            return pushOperation;
        }

        public PopOperation PopAll(TransitionAnimation transitionAnimation = null, bool asyncLoad = true)
        {
            return Pop(transitionAnimation, _scenes.Count, asyncLoad: asyncLoad);
        }

        public PopOperation PopTo(int toIndex, TransitionAnimation transitionAnimation = null, bool asyncLoad = true)
        {
            var fromScene = GetScene(-1);
            return Pop(transitionAnimation, fromScene.Index - toIndex, asyncLoad);
        }

        public PopOperation Pop(TransitionAnimation transitionAnimation = null, int times = 1, bool asyncLoad = true)
        {
            // Fix times count
            times = Math.Min(times, _scenes.Count);
            var popOperation = new _PopOperation();
            StartCoroutine(_PopCoroutine(times, popOperation, transitionAnimation, asyncLoad));
            return popOperation;
        }

        public RemoveOperation Remove(params SceneInfo[] scenes)
        {
            var operation = new _RemoveOperation();
            StartCoroutine(_RemoveCoroutine(scenes, operation));
            return operation;
        }

        void Awake()
        {
            // Handle inspector settings
            if (_setShared) _shared = this;

            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            _isDestroyed = true;
        }

        void LateUpdate()
        {
            _loadedScenes.Clear(); // Clear scenes after yield navigation coroutines
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _loadedScenes.Add(scene); // Save before yield navigation coroutines
        }

        TransitionAnimator _FindTransitionAnimator(SceneInfo scene, TransitionAnimation animation)
        {
            if (scene == null) return null;
            TransitionAnimator result = TransitionAnimator.AllAnimators.Find((animator) => _CheckSceneName(scene.Name, animator.gameObject.scene) && animator.CanAnimate(animation))
            ?? FallbackAnimator?.Find((animator) => _CheckSceneName(scene.Name, animator.gameObject.scene) && animator.CanAnimate(animation));
            if (result == null) { Debug.Log("Navigator not  found a animator"); }
            return result;
        }

        bool _CheckSceneName(string sceneName, Scene scene) => scene.path.EndsWith(sceneName + ".unity");

        IEnumerator _PushFromCoroutine<TController>(SceneInfo fromSceneInfo, string toSceneName, Scene toUnityScene, PresentationMode mode, TransitionAnimation animation, Action<TController> initializer, IEnumerator afterHide, _PushOperation<TController> pushOperation, bool asyncLoad) where TController : SceneController
        {
            var fromScene = (_SceneInfo)(fromSceneInfo ?? GetScene(-1));

            var toScene = new _SceneInfo
            {
                Index = fromScene != null ? fromScene.Index + 1 : 0,
                Name = toSceneName,
                Level = fromScene != null ? fromScene.Level + (mode == PresentationMode.ReplacePrevious ? 0 : 1) : 0,
                PresentationMode = mode,
                Initializer = c => initializer?.Invoke((TController)c)
            };

            // Start scene loading if needed 
            if (toUnityScene == default && asyncLoad)
            {
                pushOperation._LoadOperation = SceneManager.LoadSceneAsync(toSceneName, LoadSceneMode.Additive);
                pushOperation._LoadOperation.allowSceneActivation = false;
                pushOperation._BlockAllowingSceneActivation = true;
            }

            // Call navigate event
            _SafeInvoke(OnPush, s => s(this, fromScene, toScene));

            // Apply changes to _scenes
            List<_SceneInfo> scenesToRemove = fromScene != null ? _scenes.Skip(fromScene.Index + 1).ToList() : new List<_SceneInfo>();
            List<_SceneInfo> scenesToUnload;
            List<_SceneInfo> scenesToDisappear;
            List<_SceneInfo> scenesToAppear;
            _ApplyChanges(toScene, scenesToRemove, out scenesToUnload, out scenesToDisappear, out scenesToAppear);

            TransitionAnimator fromSceneAnimator = _FindTransitionAnimator(fromScene, animation);

            TransitionContext transitionContext = new TransitionContext()
            {
                Animation = animation,
                FromAnimator = fromSceneAnimator
            };

            // If serial hide animation
            if (fromSceneAnimator != null && fromSceneAnimator.GetMode(animation) == TransitionMode.Serial)
            {
                // Call SceneWillDisappear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneWillDisappear, s => s(this, sc)));

                // Run hide animation
                yield return fromSceneAnimator.AnimateTransition(TransitionStep.ForwardHide, transitionContext);
                if (afterHide != null)
                    yield return afterHide;

                // Call SceneDidDisappear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneDidDisappear, s => s(this, sc)));
            }

            if (!asyncLoad)
            {
                pushOperation._LoadOperation = SceneManager.LoadSceneAsync(toSceneName, LoadSceneMode.Additive);
            }

            // Wait for load toScene
            if (pushOperation._LoadOperation != null)
            {
                pushOperation._BlockAllowingSceneActivation = false;
                pushOperation._LoadOperation.allowSceneActivation = pushOperation.AllowSceneActivation;
                yield return pushOperation._LoadOperation;

                toUnityScene = _loadedScenes.Find(sc => _CheckSceneName(toSceneName, sc));
                _loadedScenes.Remove(toUnityScene);
            }

            // Run scene initializer
            var controller = (TController)SceneController.AllControllers.Find(c => c.gameObject.scene == toUnityScene);
            toScene.Controller = controller;
            toScene.Data = controller.SceneData;
            toScene.Initializer(controller);

            // Update push operation
            pushOperation._SceneController = controller;

            // Wait allow scene appearing
            pushOperation._ReadyForSceneAppearing = true;
            while (!pushOperation.AllowSceneAppearing)
                yield return null;

            TransitionAnimator toSceneAnimator = _FindTransitionAnimator(toScene, animation);
            transitionContext.ToAnimator = toSceneAnimator;
            // If parallel hide animation
            if (toSceneAnimator != null && toSceneAnimator.GetMode(animation) == TransitionMode.Parallel)
            {
                // Call SceneWillDisappear and SceneWillAppear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneWillDisappear, s => s(this, sc)));
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneWillAppear, s => s(this, sc)));

                // Run hide and show animations
                if (toSceneAnimator == null)
                {
                    yield return toSceneAnimator.AnimateTransition(TransitionStep.ForwardHide, transitionContext);
                }
                else
                {
                    yield return CoroutineHelper.WaitForAll(this,
                        fromSceneAnimator.AnimateTransition(TransitionStep.ForwardHide, transitionContext),
                        toSceneAnimator.AnimateTransition(TransitionStep.ForwardShow, transitionContext));
                    if (afterHide != null)
                        yield return afterHide;
                }

                // Unload all poped scenes
                if (UnloadFilter != null) scenesToUnload.RemoveAll(sc => !UnloadFilter(sc));
                yield return CoroutineHelper.WaitForAll(this, scenesToUnload.Select(sc => _UnloadCoroutine(sc)).ToArray());

                // Call SceneDidDisappear and SceneDidAppear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneDidDisappear, s => s(this, sc)));
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneDidAppear, s => s(this, sc)));
            }
            else
            {
                // Unload from scene
                if (mode == PresentationMode.ReplacePrevious)
                    yield return CoroutineHelper.WaitForAll(this, scenesToUnload.Select(sc => _UnloadCoroutine(sc)).ToArray());

                // Call SceneWillAppear
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneWillAppear, s => s(this, sc)));

                // Run show animation
                if (toSceneAnimator != null)
                    yield return toSceneAnimator.AnimateTransition(TransitionStep.ForwardShow, transitionContext);

                // Call SceneDidAppear
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneDidAppear, s => s(this, sc)));
            }

            pushOperation.OnComplete();
        }

        IEnumerator _PopCoroutine(int times, _PopOperation popOperation, TransitionAnimation animation, bool asyncLoad)
        {
            var fromScene = (_SceneInfo)GetScene(-1);
            var toScene = (_SceneInfo)GetScene(-times - 1);

            // Start scene loading if needed 
            if (toScene != null && !toScene.IsLoaded && asyncLoad)
            {
                popOperation._LoadOperation = SceneManager.LoadSceneAsync(toScene.Name, LoadSceneMode.Additive);
                popOperation._LoadOperation.allowSceneActivation = false;
            }

            // Call navigate event
            _SafeInvoke(OnPop, s => s(this, fromScene, toScene));

            // Apply changes to _scenes
            List<_SceneInfo> scenesToRemove = _scenes.Skip(_scenes.Count - times).ToList();
            List<_SceneInfo> scenesToUnload;
            List<_SceneInfo> scenesToDisappear;
            List<_SceneInfo> scenesToAppear;
            _ApplyChanges(null, scenesToRemove, out scenesToUnload, out scenesToDisappear, out scenesToAppear);


            TransitionAnimator fromSceneAnimator = _FindTransitionAnimator(fromScene, animation);
            TransitionAnimator toSceneAnimator = _FindTransitionAnimator(toScene, animation);

            TransitionContext transitionContext = new TransitionContext()
            {
                Animation = animation,
                FromAnimator = fromSceneAnimator,
                ToAnimator = toSceneAnimator
            };

            // If serial hide animation
            if (fromSceneAnimator != null && fromSceneAnimator.GetMode(animation) == TransitionMode.Serial)
            {
                // Pause scene activation
                if (popOperation._LoadOperation != null)
                    popOperation._LoadOperation.allowSceneActivation = false;

                // Call SceneWillDisappear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneWillDisappear, s => s(this, sc)));

                // Run hide animation
                yield return fromSceneAnimator.AnimateTransition(TransitionStep.BackwardHide, transitionContext);

                // Call SceneDidDisappear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneDidDisappear, s => s(this, sc)));
            }

            if (!asyncLoad)
                popOperation._LoadOperation = SceneManager.LoadSceneAsync(toScene.Name, LoadSceneMode.Additive);

            // Wait for load toScene
            if (popOperation._LoadOperation != null)
            {
                popOperation._LoadOperation.allowSceneActivation = true;
                yield return popOperation._LoadOperation;

                // Get loaded scene
                string scenePath = toScene.Name + ".unity";
                var scene = _loadedScenes.Find(sc => sc.name == toScene.Name || sc.path.EndsWith(scenePath));
                _loadedScenes.Remove(scene);

                // Run scene initializer
                var controller = SceneController.AllControllers.Find(c => c.gameObject.scene == scene);
                foreach (var pair in toScene.Data)
                {
                    controller.SceneData[pair.Key] = pair.Value;
                }
                toScene.Controller = controller;
                toScene.Initializer(controller);
            }

            // If parallel hide animation
            if (fromSceneAnimator != null && fromSceneAnimator.GetMode(animation) == TransitionMode.Parallel)
            {
                // Call SceneWillDisappear and SceneWillAppear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneWillDisappear, s => s(this, sc)));
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneWillAppear, s => s(this, sc)));

                // Run hide and show animations
                if (toSceneAnimator == null)
                    yield return fromSceneAnimator.AnimateTransition(TransitionStep.BackwardHide, transitionContext);
                else
                {
                    yield return CoroutineHelper.WaitForAll(this,
                        fromSceneAnimator.AnimateTransition(TransitionStep.BackwardHide, transitionContext),
                        toSceneAnimator.AnimateTransition(TransitionStep.BackwardShow, transitionContext));
                }

                // Unload all poped scenes
                if (UnloadFilter != null) scenesToUnload.RemoveAll(sc => !UnloadFilter(sc));
                yield return CoroutineHelper.WaitForAll(this, scenesToUnload.Select(sc => _UnloadCoroutine(sc)).ToArray());

                // Call SceneDidDisappear and SceneDidAppear
                scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidDisappear()));
                scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneDidDisappear, s => s(this, sc)));
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneDidAppear, s => s(this, sc)));
            }
            else
            {
                // Unload all poped scenes
                if (UnloadFilter != null) scenesToUnload.RemoveAll(sc => !UnloadFilter(sc));
                yield return CoroutineHelper.WaitForAll(this, scenesToUnload.Select(sc => _UnloadCoroutine(sc)).ToArray());

                // Call SceneWillAppear
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneWillAppear, s => s(this, sc)));

                // Run show animation
                if (toSceneAnimator != null)
                    yield return toSceneAnimator.AnimateTransition(TransitionStep.BackwardShow, transitionContext);

                // Call SceneDidAppear
                scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidAppear()));
                scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneDidAppear, s => s(this, sc)));
            }

            popOperation.OnComplete();
        }

        IEnumerator _RemoveCoroutine(IEnumerable<SceneInfo> scenes, _RemoveOperation operation = null)
        {
            // Apply changes to _scenes
            List<_SceneInfo> scenesToRemove = scenes.Cast<_SceneInfo>().ToList();
            List<_SceneInfo> scenesToUnload;
            List<_SceneInfo> scenesToDisappear;
            List<_SceneInfo> scenesToAppear;
            _ApplyChanges(null, scenesToRemove, out scenesToUnload, out scenesToDisappear, out scenesToAppear);

            // Call SceneWillDisappear and SceneWillAppear
            scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillDisappear()));
            scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneWillDisappear, s => s(this, sc)));
            scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneWillAppear()));
            scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneWillAppear, s => s(this, sc)));

            // Unload scenes
            if (UnloadFilter != null) scenesToUnload.RemoveAll(sc => !UnloadFilter(sc));
            yield return CoroutineHelper.WaitForAll(this, scenesToUnload.Select(sc => _UnloadCoroutine(sc)).ToArray());

            // Call SceneDidDisappear and SceneDidAppear
            scenesToDisappear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidDisappear()));
            scenesToDisappear.ForEach(sc => _SafeInvoke(OnSceneDidDisappear, s => s(this, sc)));
            scenesToAppear.ForEach(sc => _SafeInvoke(sc.Controller, s => s.SceneDidAppear()));
            scenesToAppear.ForEach(sc => _SafeInvoke(OnSceneDidAppear, s => s(this, sc)));

            operation?.OnComplete();
        }

        IEnumerator _UnloadCoroutine(_SceneInfo scene)
        {
            yield return SceneManager.UnloadSceneAsync(scene.Controller.gameObject.scene);
            scene.Controller = null;
            yield return Resources.UnloadUnusedAssets();
        }

        void _ApplyChanges(_SceneInfo addScene, List<_SceneInfo> removeScenes,
            out List<_SceneInfo> scenesToUnload, out List<_SceneInfo> scenesToDisappear, out List<_SceneInfo> scenesToAppear)
        {
            // Find appeared scenes before changes
            var reversedScenes = _scenes.AsEnumerable().Reverse();
            var appearedScenesBeforeChanges = new List<_SceneInfo>();
            foreach (var sc in reversedScenes)
            {
                if (appearedScenesBeforeChanges.Count == 0 || appearedScenesBeforeChanges.Last().Level != sc.Level)
                    appearedScenesBeforeChanges.Add(sc);

                if (sc.PresentationMode == PresentationMode.NewOpaqueLevel)
                    break;
            }

            // Apply changes and fix indexes and levels
            int index = 0, level = -1;
            if (addScene != null)
                _scenes.Add(addScene);
            foreach (var sc in _scenes.ToList())
            {
                if (removeScenes.Contains(sc))
                    _scenes.Remove(sc);
                else
                {
                    if (sc.PresentationMode != PresentationMode.ReplacePrevious)
                        level++;

                    sc.Index = index;
                    sc.Level = level;
                    index++;
                }
            }

            // Find appeared scenes after changes
            var appearedScenesAfterChanges = new List<_SceneInfo>();
            foreach (var sc in _scenes.AsEnumerable().Reverse())
            {
                if (appearedScenesAfterChanges.Count == 0 || appearedScenesAfterChanges.Last().Level != sc.Level)
                    appearedScenesAfterChanges.Add(sc);

                if (sc.PresentationMode == PresentationMode.NewOpaqueLevel)
                    break;
            }

            // Find scenes to appear, to disappear and to unload
            scenesToDisappear = appearedScenesBeforeChanges.Except(appearedScenesAfterChanges).ToList();
            scenesToAppear = appearedScenesAfterChanges.Except(appearedScenesBeforeChanges).ToList();
            scenesToUnload = removeScenes.Where(sc => sc.IsLoaded).Reverse().ToList();
            if (addScene != null && addScene.PresentationMode == PresentationMode.ReplacePrevious)
            {
                scenesToUnload.Add(_scenes[addScene.Index - 1]);
            }
        }

        void _SafeInvoke<T>(T source, Action<T> invoker) where T : class
        {
            if (source != null)
            {
                try { invoker(source); }
                catch (Exception ex) { Debug.LogException(ex); }
            }
        }

        class _SceneInfo : SceneInfo
        {
            public int Index { get; set; }
            public string Name { get; set; }
            public int Level { get; set; }
            public PresentationMode PresentationMode { get; set; }
            public Dictionary<string, object> Data { get; set; }
            public SceneController Controller { get; set; }
            public TransitionAnimation TransitionAnimation { get; set; }

            public bool IsLoaded => Controller != null;

            public Action<SceneController> Initializer;
        }

        class _PushOperation<TController> : PushOperation<TController> where TController : SceneController
        {
            public override bool keepWaiting => _keepWaiting;
            public override bool ReadyForSceneActivation => _LoadOperation.progress >= 0.9f;
            public override bool ReadyForSceneAppearing => _ReadyForSceneAppearing;
            public override bool AllowSceneActivation
            {
                get => _AllowSceneActivation;
                set
                {
                    _AllowSceneActivation = value;
                    if (_LoadOperation != null && !_BlockAllowingSceneActivation) _LoadOperation.allowSceneActivation = value;
                }
            }
            public override bool AllowSceneAppearing { get => _AllowSceneAppearing; set => _AllowSceneAppearing = value; }
            public override float LoadProgress => _LoadOperation != null ? Mathf.Clamp01(_LoadOperation.progress / 0.9f) : 1f;
            public override TController SceneController => _SceneController;
            public override event Action Completed;

            bool _keepWaiting = true;
            public bool _ReadyForSceneAppearing = false;
            public bool _AllowSceneAppearing = true;
            public bool _AllowSceneActivation = true;
            public bool _BlockAllowingSceneActivation;

            public AsyncOperation _LoadOperation;
            public TController _SceneController;

            public void OnComplete()
            {
                _keepWaiting = false;
                Completed?.Invoke();
            }
        }

        class _PopOperation : PopOperation
        {
            public override bool keepWaiting => _keepWaiting;
            public override event Action Completed;

            bool _keepWaiting = true;
            public AsyncOperation _LoadOperation;

            public void OnComplete()
            {
                _keepWaiting = false;
                Completed?.Invoke();
            }
        }

        class _RemoveOperation : RemoveOperation
        {
            public override bool keepWaiting => _keepWaiting;
            public override event Action Completed;

            bool _keepWaiting = true;

            public void OnComplete()
            {
                _keepWaiting = false;
                Completed?.Invoke();
            }
        }
    }
}