﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LimeKit.Core;

namespace LimeKit.Core
{
    public abstract class TransitionAnimator : MonoBehaviour
    {
        public static readonly List<TransitionAnimator> AllAnimators = new List<TransitionAnimator>();


        protected virtual void Awake()
        {
            AllAnimators.Add(this);
        }

        protected virtual void OnDestroy()
        {
            AllAnimators.Remove(this);
        }

        public abstract bool CanAnimate(TransitionAnimation animation);
        public abstract IEnumerator AnimateTransition(TransitionStep step, TransitionContext transitionContext);
        public abstract TransitionMode GetMode(TransitionAnimation animation);

    }

    public struct TransitionContext
    {
        public TransitionAnimation Animation;
        public TransitionAnimator FromAnimator;
        public TransitionAnimator ToAnimator;
        public Dictionary<string, object> SharedData;
    }

    public enum TransitionMode
    {
        Serial,
        Parallel
    }

    public enum TransitionStep
    {
        ForwardHide,
        ForwardShow,
        BackwardHide,
        BackwardShow
    }
}