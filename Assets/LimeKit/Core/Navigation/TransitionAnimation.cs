﻿using System;
using System.Collections.Generic;

namespace LimeKit.Core
{
    public class TransitionAnimation
    {
        public static TransitionAnimation NoAnimation = new TransitionAnimation("NoAnimation");
        public static TransitionAnimation CrossDissolve = new TransitionAnimation("CrossDissolve");
        public static TransitionAnimation Custom(string name) => new TransitionAnimation(name);

        public string Name { get; private set; }
        public Dictionary<string, object> dataTransition = new Dictionary<string, object>();

        TransitionAnimation(string name)
        {
            Name = name;
        }

        public static TransitionAnimation GetCrossCanvas(float duration)
        {
            TransitionAnimation result = new TransitionAnimation("CrossCanvas");
            result.dataTransition.Add("duration", duration);
            return result;
        }


    }
}
