using System.Collections.Generic;
using UnityEngine;

namespace LimeKit.Core
{
    public abstract class SceneController : MonoBehaviour
    {
        public static readonly List<SceneController> AllControllers = new List<SceneController>();
        public readonly Dictionary<string, object> SceneData = new Dictionary<string, object>();

        public bool SceneAppearing { get; private set; }
        public bool SceneAppeared { get; private set; }
        public bool SceneDisappearing { get; private set; }
        public bool SceneDisappeared { get; private set; }


        protected virtual void Awake()
        {
            AllControllers.Add(this);
        }

        protected virtual void OnDestroy()
        {
            AllControllers.Remove(this);
        }

        public virtual void SceneWillAppear()
        {
            SceneAppearing = true;
            SceneAppeared = false;
        }

        public virtual void SceneDidAppear()
        {
            SceneAppearing = false;
            SceneAppeared = true;
            SceneDisappeared = false;
        }

        public virtual void SceneWillDisappear()
        {
            SceneDisappearing = true;
            SceneDisappeared = false;
        }

        public virtual void SceneDidDisappear()
        {
            SceneDisappearing = false;
            SceneDisappeared = true;
            SceneAppeared = false;
        }
    }
}