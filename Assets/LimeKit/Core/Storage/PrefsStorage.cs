﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace LimeKit.Core
{
    public class PrefsStorage : IStorage
    {
        public string KeyPrefix { get; private set; }
        public bool ForceSave { get; set; }

        readonly HashSet<string> _keys;
        readonly string _keysKey = "_PrefsStorage";
        readonly BinaryFormatter _binaryFormatter = new BinaryFormatter();

        public PrefsStorage(string keyPrefix = "", bool forceSave = false)
        {
            KeyPrefix = keyPrefix;
            ForceSave = forceSave;
            _keys = _LoadKeys();
        }

        public string GetString(string key)
        {
            string prefsKey = _PrefsKey(key);
            return PlayerPrefs.HasKey(prefsKey) ? PlayerPrefs.GetString(prefsKey) : null;
        }

        public byte[] GetBytes(string key)
        {
            string prefsKey = _PrefsKey(key);
            return PlayerPrefs.HasKey(prefsKey) ? Convert.FromBase64String(PlayerPrefs.GetString(prefsKey)) : null;
        }

        public void SetString(string key, string text)
        {
            if (text != null)
            {
                _AddKey(key);
                PlayerPrefs.SetString(_PrefsKey(key), text);
                if (ForceSave) PlayerPrefs.Save();
            }
            else Remove(key);
        }

        public void SetBytes(string key, byte[] bytes)
        {
            if (bytes != null)
            {
                _AddKey(key);
                PlayerPrefs.SetString(_PrefsKey(key), Convert.ToBase64String(bytes));
                if (ForceSave) PlayerPrefs.Save();
            }
            else Remove(key);
        }

        public bool Remove(string key)
        {
            if (_keys.Contains(key))
            {
                PlayerPrefs.DeleteKey(_PrefsKey(key));
                if (ForceSave) PlayerPrefs.Save();
                _keys.Remove(key);
                _SaveKeys();
                return true;
            }
            else return false;
        }

        public void RemoveAll()
        {
            foreach (var key in _keys)
            {
                PlayerPrefs.DeleteKey(_PrefsKey(key));
            }
            if (ForceSave) PlayerPrefs.Save();
            _keys.Clear();
            _SaveKeys();
        }

        public bool ContainsKey(string key)
        {
            return _keys.Contains(key);
        }

        public IEnumerable<string> EnumerateKeys()
        {
            return _keys;
        }

        public int Count()
        {
            return _keys.Count;
        }

        HashSet<string> _LoadKeys()
        {
            string keysPrefsKey = KeyPrefix + _keysKey;
            if (PlayerPrefs.HasKey(keysPrefsKey))
            {
                var bytes = Convert.FromBase64String(PlayerPrefs.GetString(keysPrefsKey));
                using(var memoryStream = new MemoryStream(bytes))
                {
                    return _binaryFormatter.Deserialize(memoryStream) as HashSet<string>;
                }
            }
            else return new HashSet<string>();
        }

        void _SaveKeys()
        {
            using(var memoryStream = new MemoryStream())
            {
                _binaryFormatter.Serialize(memoryStream, _keys);
                PlayerPrefs.SetString(KeyPrefix + _keysKey, Convert.ToBase64String(memoryStream.ToArray()));
                if (ForceSave) PlayerPrefs.Save();
            }
        }

        void _AddKey(string key)
        {
            if (key == _keysKey) throw new Exception("Key \"" + key + "\" is reserved");
            if (!_keys.Contains(key))
            {
                _keys.Add(key);
                _SaveKeys();
            }
        }

        string _PrefsKey(string cacheKey) => $"{KeyPrefix}_{cacheKey}";
    }
}