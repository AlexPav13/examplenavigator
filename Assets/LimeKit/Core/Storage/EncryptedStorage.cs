﻿using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace LimeKit.Core
{
    public class EncryptedStorage : IStorage
    {
        readonly IStorage _storage;
        readonly byte[] _passwordBytes;

        public EncryptedStorage(IStorage storage, string password)
        {
            _storage = storage;
            _passwordBytes = Encoding.ASCII.GetBytes(password);
        }

        public string GetString(string key)
        {
            if (_storage.ContainsKey(key))
            {
                using(var aes = new AesManaged())
                {
                    ICryptoTransform decryptor = aes.CreateDecryptor(_passwordBytes, _passwordBytes);
                    using(var inputStream = new MemoryStream(_storage.GetBytes(key)))
                    using(var cryptoStream = new CryptoStream(inputStream, decryptor, CryptoStreamMode.Read))
                    using(StreamReader resultStream = new StreamReader(cryptoStream))
                    {
                        return resultStream.ReadToEnd();
                    }
                }
            }
            else return null;
        }

        public byte[] GetBytes(string key)
        {
            return _storage.ContainsKey(key) ? Encoding.ASCII.GetBytes(GetString(key)) : null;
        }

        public void SetString(string key, string text)
        {
            if (text != null)
                SetBytes(key, Encoding.ASCII.GetBytes(text));
            else
                Remove(key);
        }

        public void SetBytes(string key, byte[] bytes)
        {
            if (bytes != null)
            {
                using(var aes = new AesManaged())
                {
                    ICryptoTransform encryptor = aes.CreateEncryptor(_passwordBytes, _passwordBytes);
                    using(var memoryStream = new MemoryStream())
                    using(var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using(var sw = new StreamWriter(cryptoStream))
                        {
                            sw.Write(bytes);
                        }
                        _storage.SetBytes(key, memoryStream.ToArray());
                    }
                }
            }
            else Remove(key);
        }

        public bool Remove(string key) => _storage.Remove(key);

        public void RemoveAll() => _storage.RemoveAll();

        public bool ContainsKey(string key) => _storage.ContainsKey(key);

        public IEnumerable<string> EnumerateKeys() => _storage.EnumerateKeys();

        public int Count() => _storage.Count();
    }
}