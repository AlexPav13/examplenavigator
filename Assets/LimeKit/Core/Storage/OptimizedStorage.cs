﻿using System.Collections.Generic;

namespace LimeKit.Core
{
    public class OptimizedStorage : IStorage
    {
        readonly IStorage _regularStorage;
        readonly IStorage _fastStorage;

        public OptimizedStorage(IStorage regularStorage, IStorage fastStorage)
        {
            _regularStorage = regularStorage;
            _fastStorage = fastStorage;
        }

        public string GetString(string key)
        {
            return _fastStorage.GetString(key) ?? _regularStorage.GetString(key);
        }

        public byte[] GetBytes(string key)
        {
            return _fastStorage.GetBytes(key) ?? _regularStorage.GetBytes(key);
        }

        public void SetString(string key, string text)
        {
            _regularStorage.SetString(key, text);
            _fastStorage.SetString(key, text);
        }

        public void SetBytes(string key, byte[] bytes)
        {
            _regularStorage.SetBytes(key, bytes);
            _fastStorage.SetBytes(key, bytes);
        }

        public bool Remove(string key)
        {
            _fastStorage.Remove(key);
            return _regularStorage.Remove(key);
        }

        public void RemoveAll()
        {
            _fastStorage.RemoveAll();
            _regularStorage.RemoveAll();
        }

        public bool ContainsKey(string key)
        {
            return _fastStorage.ContainsKey(key) || _regularStorage.ContainsKey(key);
        }

        public IEnumerable<string> EnumerateKeys()
        {
            return _regularStorage.EnumerateKeys();
        }

        public int Count() => _regularStorage.Count();
    }
}