using System.Collections.Generic;

namespace LimeKit.Core
{
    // Not implemented

    public class PurgeableStorage : IStorage
    {
        public long MaxSize;
        public double MaxAge;

        readonly IStorage _storage;

        public PurgeableStorage(IStorage storage, long maxSize = -1, double maxAge = -1)
        {
            MaxSize = maxSize;
            MaxAge = maxAge;
            _storage = storage;

            throw new System.NotImplementedException();
        }

        public string GetString(string key) => _storage.GetString(key);

        public byte[] GetBytes(string key) => _storage.GetBytes(key);

        public void SetString(string key, string text)
        {
            _storage.SetString(key, text);
            Purge();
        }

        public void SetBytes(string key, byte[] bytes)
        {
            _storage.SetBytes(key, bytes);
            Purge();
        }

        public bool ContainsKey(string key) => _storage.ContainsKey(key);

        public bool Remove(string key) => _storage.Remove(key);

        public void RemoveAll() => _storage.RemoveAll();

        public IEnumerable<string> EnumerateKeys() => _storage.EnumerateKeys();

        public int Count() => _storage.Count();

        public long GetTotalSize()
        {
            throw new System.NotImplementedException();
        }

        public void Purge()
        {
            throw new System.NotImplementedException();
        }
    }
}