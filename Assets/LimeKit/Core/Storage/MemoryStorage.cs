﻿using System.Collections.Generic;
using System.Text;

namespace LimeKit.Core
{
    public class MemoryStorage : IStorage
    {
        readonly Dictionary<string, byte[]> _memoryMap = new Dictionary<string, byte[]>();

        public string GetString(string key)
        {
            return _memoryMap.ContainsKey(key) ? Encoding.ASCII.GetString(_memoryMap[key]) : null;
        }

        public byte[] GetBytes(string key)
        {
            return _memoryMap.ContainsKey(key) ? _memoryMap[key] : null;
        }

        public void SetString(string key, string text)
        {
            if (text != null)
                _memoryMap.Add(key, Encoding.ASCII.GetBytes(text));
            else
                Remove(key);
        }

        public void SetBytes(string key, byte[] bytes)
        {
            if (bytes != null)
                _memoryMap.Add(key, bytes);
            else
                Remove(key);
        }

        public bool Remove(string key) => _memoryMap.Remove(key);

        public void RemoveAll() => _memoryMap.Clear();

        public bool ContainsKey(string key) => _memoryMap.ContainsKey(key);

        public IEnumerable<string> EnumerateKeys() => _memoryMap.Keys;

        public int Count() => _memoryMap.Keys.Count;
    }
}