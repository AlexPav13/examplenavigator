using System.Collections.Generic;

namespace LimeKit.Core
{
    public interface IStorage
    {
        string GetString(string key);
        byte[] GetBytes(string key);

        void SetString(string key, string text);
        void SetBytes(string key, byte[] bytes);

        bool Remove(string key);
        void RemoveAll();

        bool ContainsKey(string key);
        IEnumerable<string> EnumerateKeys();
        int Count();
    }
}