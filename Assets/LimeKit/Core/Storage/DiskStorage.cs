﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LimeKit.Core
{
    public class DiskStorage : IStorage
    {
        public string DirectoryPath { get; private set; }

        public DiskStorage(string directoryPath)
        {
            DirectoryPath = directoryPath;
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
        }

        public string GetString(string key)
        {
            var filepath = _KeyToFilePath(key);
            return File.Exists(filepath) ? File.ReadAllText(filepath) : null;
        }

        public byte[] GetBytes(string key)
        {
            var filepath = _KeyToFilePath(key);
            return File.Exists(filepath) ? File.ReadAllBytes(filepath) : null;
        }

        public void SetString(string key, string text)
        {
            if (text != null)
            {
                var filepath = _KeyToFilePath(key);
                File.WriteAllText(filepath, text);
            }
            else Remove(key);
        }

        public void SetBytes(string key, byte[] bytes)
        {
            if (bytes != null)
            {
                var filepath = _KeyToFilePath(key);
                File.WriteAllBytes(filepath, bytes);
            }
            else Remove(key);
        }

        public bool Remove(string key)
        {
            var filepath = _KeyToFilePath(key);
            bool exist = File.Exists(filepath);
            File.Delete(filepath);
            return exist;
        }

        public void RemoveAll()
        {
            foreach (var filepath in Directory.GetFiles(DirectoryPath))
                File.Delete(filepath);
        }

        public bool ContainsKey(string key)
        {
            var filepath = _KeyToFilePath(key);
            return File.Exists(filepath);
        }

        public IEnumerable<string> EnumerateKeys()
        {
            return Directory.GetFiles(DirectoryPath).Select(_FilePathToKey);
        }

        public int Count()
        {
            return Directory.GetFiles(DirectoryPath).Length;
        }

        string _KeyToFilePath(string key)
        {
            return Path.Combine(this.DirectoryPath, Uri.EscapeDataString(key));
        }

        string _FilePathToKey(string filepath)
        {
            return Uri.UnescapeDataString(Path.GetFileName(filepath));
        }
    }
}