using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LimeKit.Core
{
    public abstract class Context<TContext> : MonoBehaviour where TContext : MonoBehaviour
    {
        readonly static Dictionary<string, Func<object>> resolvers = new Dictionary<string, Func<object>>();
        readonly static Dictionary<string, object> singletons = new Dictionary<string, object>();

        public static void Register<T>(T instance, bool weak = false, string tag = null) where T : class
        {
            // Check if registration already exists
            string key = KeyFor(typeof(T), tag);
            if (Contains<T>(key))
            {
                Remove<T>(key);
                Debug.LogWarning($"[{typeof(TContext)}] Registration for type " + typeof(T) + " is overwritten");
            }

            // Create resolver for type
            Func<T> resolver;
            if (weak)
            {
                WeakReference weakRef = new WeakReference(instance);
                resolver = () => weakRef.Target as T;
            }
            else
            {

                singletons[key] = instance;
                if (instance is MonoBehaviour)
                {
                    UnityEngine.Object.DontDestroyOnLoad((instance as MonoBehaviour).gameObject);
                }
                resolver = () => singletons[key] as T;
            }

            // Save resolver with thread-safety handling
            lock(resolvers)
            {
                resolvers[key] = resolver as Func<object>;
            }

            Debug.Log($"[{typeof(TContext)}] Registration for type {typeof(T)} was created");
        }

        public static void RegisterFactory<T>(Func<T> factory, bool singleton = false, string tag = null) where T : class
        {
            RegisterFactory<T, T>(factory, singleton, tag);
        }

        public static void RegisterFactory<T, R>(Func<R> factory, bool singleton = false, string tag = null) where T : class where R : class, T
        {
            // Check if registration already exists
            string key = KeyFor(typeof(T), tag);
            if (Contains<T>(key))
            {
                Remove<T>(key);
                Debug.LogWarning($"[{typeof(TContext)}] Registration for type " + typeof(T) + " is overwritten");
            }

            // Create resolver for type
            Func<R> resolver;
            if (singleton)
            {
                resolver = () =>
                {
                    if (!singletons.ContainsKey(key))
                    {
                        // Create and save singleton instance
                        var instance = factory();
                        if (instance is MonoBehaviour)
                        {
                            UnityEngine.Object.DontDestroyOnLoad((instance as MonoBehaviour).gameObject);
                        }
                        Debug.Log($"[{typeof(TContext)}] Singleton instance of {typeof(R)} was created for type {typeof(T)}");
                        singletons[key] = instance;
                    }
                    return singletons[key] as R;
                };
            }
            else
            {
                resolver = () =>
                {
                    var instance = factory();
                    Debug.Log($"[{typeof(TContext)}] New instance of {typeof(R)} was created for type {typeof(T)}");
                    return instance;
                };
            }

            // Save resolver with thread-safety handling
            lock(resolvers)
            {
                resolvers[key] = resolver as Func<object>;
            }
            Debug.Log($"[{typeof(TContext)}] Registration for type {typeof(T)} was created");
        }

        public static T Get<T>(string tag = null) where T : class
        {
            string key = KeyFor(typeof(T), tag);
            T instance = null;
            // Get instance with thread-safety handling
            lock(resolvers)
            {
                if (resolvers.ContainsKey(key))
                    instance = resolvers[key]() as T;
                else
                    Debug.LogWarning($"[{typeof(TContext)}] Registration for type {typeof(T)} not found");
            }
            return instance;
        }

        public static Lazy<T> GetLazy<T>(string tag = null) where T : class
        {
            return new Lazy<T>(() => Get<T>(), true);
        }

        public static void Remove<T>(string tag = null) where T : class
        {
            string key = KeyFor(typeof(T), tag);
            // Remove registration with thread-safety handling
            lock(resolvers)
            {
                if (singletons.ContainsKey(key) && singletons[key] is MonoBehaviour)
                {
                    // Destroy MonoBehaviour singleton
                    var gameObject = (singletons[key] as MonoBehaviour).gameObject;
                    if (Application.isEditor)
                    {
                        UnityEngine.Object.DestroyImmediate(gameObject);
                    }
                    else
                    {
                        UnityEngine.Object.Destroy(gameObject);
                    }
                    Debug.Log($"[{typeof(TContext)}] Singleton MonoBehaviour instance of {singletons[key].GetType()} for type {typeof(T)} was destroyed");
                }
                singletons[key] = null;
                resolvers[key] = null;
            }
            Debug.Log($"[{typeof(TContext)}] Registration for type {typeof(T)} was removed");
        }

        public static bool Contains<T>(string tag = null) where T : class
        {
            string key = KeyFor(typeof(T), tag);
            return resolvers.ContainsKey(key);
        }

        public static IEnumerator WaitForRegister<T>(string tag = null) where T : class
        {
            while (!Contains<T>(tag))
                yield return null;
        }

        static string KeyFor(Type type, string tag)
        {
            return type.Name + (tag != null ? "@" + tag : "");
        }
    }
}