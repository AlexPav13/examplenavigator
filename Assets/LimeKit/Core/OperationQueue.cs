using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LimeKit.Core
{
    public enum OperationPriority
    {
        Low,
        Medium,
        High
    }

    public class OperationQueue : MonoBehaviour
    {
        public delegate void AsyncOperation(Action onComplete);

        public int MaxConcurrentOperationsCount = -1;
        public int OperationsCount => _queue.Count;
        public int PendingOperationsCount { get; private set; }

        readonly List<_OperationInfo> _queue = new List<_OperationInfo>();

        public void Add(Action operation, OperationPriority priority = OperationPriority.Medium)
        {
            Add(onComplete =>
            {
                operation();
                onComplete();
            }, priority);
        }

        public void Add(AsyncOperation operation, OperationPriority priority = OperationPriority.Medium)
        {
            _queue.Add(new _OperationInfo { Execute = operation, Priority = priority });
        }

        public void Add(IEnumerator operation, OperationPriority priority = OperationPriority.Medium)
        {
            Add(onComplete => CoroutineHelper.StartCoroutine(this, operation, onComplete), priority);
        }

        void Update()
        {
            if (_queue.Count == 0 || PendingOperationsCount >= MaxConcurrentOperationsCount)
                return;

            var operationsToPerform = _queue.OrderByDescending(op => op.Priority)
                .Take(MaxConcurrentOperationsCount - PendingOperationsCount);
            foreach (var op in operationsToPerform)
            {
                var refCopy = op;
                op.Execute(onComplete: () => _queue.Remove(refCopy));
            }
        }

        struct _OperationInfo
        {
            public AsyncOperation Execute;
            public OperationPriority Priority;
        }
    }
}