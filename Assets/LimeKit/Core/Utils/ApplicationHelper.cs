﻿using System;
using UnityEngine;

namespace LimeKit.Core
{
    public static class ApplicationHelper
    {
        public static void OpenAppStoreReviewPage(string appleId, string locale = "us")
        {
            try
            {
                string[] ver = UnityEngine.iOS.Device.systemVersion.Split('.');
                if (int.Parse(ver[0]) >= 11)
                    Application.OpenURL("itms-apps://itunes.apple.com/" + locale + "/app/itunes-u/id" + appleId + "?action=write-review");
                else
                    Application.OpenURL("itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=" + appleId);
            }
            catch { }
        }

        public static void OpenAppStorePage(string appleId)
        {
            Application.OpenURL("itms-apps://itunes.apple.com/app/id" + appleId);
        }

        public static void OpenGooglePlayPage(string packageName)
        {
            Application.OpenURL("market://details?id=" + packageName);
        }

        public static void OpenMailTo(string email, string subject, string body)
        {
            Application.OpenURL(
                "mailto:" + email +
                "?subject=" + Uri.EscapeUriString(subject) +
                "&body=" + Uri.EscapeUriString(body));
        }
    }
}