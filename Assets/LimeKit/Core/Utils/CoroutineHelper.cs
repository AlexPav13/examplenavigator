using System;
using System.Collections;
using UnityEngine;

namespace LimeKit.Core
{
    public static class CoroutineHelper
    {
        public static void StartCoroutine(MonoBehaviour owner, IEnumerator routine, Action onComplete)
        {
            owner.StartCoroutine(_Observer(routine, onComplete));
        }

        public static IEnumerator WaitForAll(MonoBehaviour owner, params IEnumerator[] routines)
        {
            int counter = 0;
            for (int i = 0; i < routines.Length; i++)
            {
                StartCoroutine(owner, routines[i], () => counter++);
            }
            while (counter < routines.Length)
                yield return null;
        }

        static IEnumerator _Observer(IEnumerator routine, Action onComplete)
        {
            yield return routine;
            onComplete?.Invoke();
        }
    }
}