using UnityEngine;

namespace LimeKit.Core
{
    public static class VectorExtensions
    {
        public static Vector2 WithX(this Vector2 vector, float x)
        {
            vector.x = x;
            return vector;
        }

        public static Vector2 WithAddX(this Vector2 vector, float x)
        {
            vector.x += x;
            return vector;
        }

        public static Vector2 WithY(this Vector2 vector, float y)
        {
            vector.y = y;
            return vector;
        }

        public static Vector2 WithAddY(this Vector2 vector, float y)
        {
            vector.y += y;
            return vector;
        }

        public static Vector3 WithX(this Vector3 vector, float x)
        {
            vector.x = x;
            return vector;
        }

        public static Vector3 WithAddX(this Vector3 vector, float x)
        {
            vector.x += x;
            return vector;
        }

        public static Vector3 WithY(this Vector3 vector, float y)
        {
            vector.y = y;
            return vector;
        }

        public static Vector3 WithAddY(this Vector3 vector, float y)
        {
            vector.y += y;
            return vector;
        }

        public static Vector3 WithZ(this Vector3 vector, float z)
        {
            vector.z = z;
            return vector;
        }

        public static Vector3 WithAddZ(this Vector3 vector, float z)
        {
            vector.z += z;
            return vector;
        }

        public static Vector3 WithXY(this Vector3 vector, float xy)
        {
            vector.x = xy;
            vector.y = xy;
            return vector;
        }

        public static Vector3 WithAddXY(this Vector3 vector, float xy)
        {
            vector.x += xy;
            vector.y += xy;
            return vector;
        }
    }
}