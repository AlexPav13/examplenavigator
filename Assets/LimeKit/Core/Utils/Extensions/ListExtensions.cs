using System.Collections.Generic;
using UnityEngine;

namespace LimeKit.Core
{
    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            for (int i = list.Count - 1; i > 0; i--)
            {
                int r = Random.Range(0, i);
                T tmp = list[i];
                list[i] = list[r];
                list[r] = tmp;
            }
        }
    }
}