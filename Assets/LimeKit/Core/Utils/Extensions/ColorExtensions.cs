using UnityEngine;
using UnityEngine.UI;

namespace LimeKit.Core
{
    public static class ColorExtensions
    {
        public static void SetColorAlpha(this Graphic graphic, float a)
        {
            graphic.color = graphic.color.WithAlpha(a);
        }

        public static Color WithRed(this Color color, float r)
        {
            color.r = r;
            return color;
        }

        public static Color WithGreen(this Color color, float g)
        {
            color.g = g;
            return color;
        }

        public static Color WithBlue(this Color color, float b)
        {
            color.b = b;
            return color;
        }

        public static Color WithAlpha(this Color color, float a)
        {
            color.a = a;
            return color;
        }
    }
}