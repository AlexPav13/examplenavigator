using System;
using UnityEngine;

namespace LimeKit.Core
{
    public sealed class Promise<T> : CustomYieldInstruction
    {
        public static Promise<T> Rejected(Exception error) { return new Promise<T>() { IsCompleted = true, Error = error, Progress = 1.0f }; }
        public static Promise<T> Resolved(T value) { return new Promise<T>() { IsCompleted = true, Value = value, Progress = 1.0f }; }

        public bool IsCompleted { get; private set; }
        public bool IsCanceled { get; private set; }
        public T Value { get; private set; }
        public Exception Error { get; private set; }
        public float Progress { get; private set; }
        public override bool keepWaiting => !IsCompleted && !IsCanceled;

        public event Action<float> ProgressChanged;
        public event Action<T, Exception> Completed;
        public event Action Canceled;

        public void Resolve(T value)
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled)
                    return;

                Value = value;
                IsCompleted = true;
                if (Progress < 1) ReportProgress(1.0f);
                Completed?.Invoke(Value, Error);
            }
        }

        public void Reject(Exception error)
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled)
                    return;

                Error = error;
                IsCompleted = true;
                if (Progress < 1) ReportProgress(1.0f);
                Completed?.Invoke(Value, Error);
            }
        }

        public void ReportProgress(float progress)
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled) return;

                Progress = progress;
                ProgressChanged?.Invoke(progress);
            }
        }

        public void Cancel()
        {
            lock(this)
            {
                if (!IsCanceled)
                {
                    IsCanceled = true;
                    Canceled?.Invoke();
                }
            }
        }
    }

    public sealed class Promise : CustomYieldInstruction
    {
        public static Promise Rejected(Exception error) { return new Promise() { IsCompleted = true, Error = error, Progress = 1.0f }; }
        public static Promise Resolved() { return new Promise() { IsCompleted = true, Progress = 1.0f }; }

        public bool IsCompleted { get; private set; }
        public bool IsCanceled { get; private set; }
        public Exception Error { get; private set; }
        public float Progress { get; private set; }
        public override bool keepWaiting => !IsCompleted && !IsCanceled;

        public event Action<float> ProgressChanged;
        public event Action<Exception> Completed;
        public event Action Canceled;

        public void Resolve()
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled)
                    return;
                IsCompleted = true;
                if (Progress < 1) ReportProgress(1.0f);
                Completed?.Invoke(Error);
            }
        }

        public void Reject(Exception error)
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled)
                    return;
                Error = error;
                IsCompleted = true;
                if (Progress < 1) ReportProgress(1.0f);
                Completed?.Invoke(Error);
            }
        }

        public void ReportProgress(float progress)
        {
            lock(this)
            {
                if (IsCompleted || IsCanceled)
                    return;
                Progress = progress;
                ProgressChanged?.Invoke(progress);
            }
        }

        public void Cancel()
        {
            lock(this)
            {
                if (!IsCanceled)
                {
                    IsCanceled = true;
                    Canceled?.Invoke();
                }
            }
        }
    }
}