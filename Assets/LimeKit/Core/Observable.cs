using System;

namespace LimeKit.Core
{
    public interface IObservable<T>
    {
        T Value { get; }
        event Action<T> ValueChanged;
    }

    public class Observable<T> : IObservable<T>
    {
        public event Action<T> ValueChanged;

        T _value;
        public T Value
        {
            get => _value;
            set
            {
                _value = value;
                ValueChanged?.Invoke(value);
            }
        }

        public Observable(T initialValue = default)
        {
            _value = initialValue;
        }
    }
}