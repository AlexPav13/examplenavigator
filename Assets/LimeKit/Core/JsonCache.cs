using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LimeKit.Core
{
    public class JsonCache
    {
        readonly IStorage _storage;

        public JsonCache(IStorage storage)
        {
            _storage = storage;
        }

        public JsonCache(string dataFolder = "data")
        {
            _storage = new DiskStorage(Application.persistentDataPath + "/" + dataFolder);
        }

        public TSerializable LoadObject<TSerializable>(string tag = "default")
        {
            string key = _KeyFor<TSerializable>(tag);
            if (_storage.ContainsKey(key))
                return JsonUtility.FromJson<TSerializable>(_storage.GetString(key));
            else
                return default;
        }

        public void LoadObject<TSerializable>(TSerializable objectToOverwrite, string tag = "default")
        {
            string key = _KeyFor<TSerializable>(tag);
            if (_storage.ContainsKey(key))
                JsonUtility.FromJsonOverwrite(_storage.GetString(key), objectToOverwrite);
        }

        public TSerializable[] LoadArray<TSerializable>(string tag = "default")
        {
            string key = _KeyFor<TSerializable>(tag);
            if (_storage.ContainsKey(key))
                return JsonUtility.FromJson<Wrapper<TSerializable>>(_storage.GetString(key)).Items;
            else
                return default;
        }

        public void SaveObject<TSerializable>(TSerializable data, string tag = "default")
        {
            string key = _KeyFor<TSerializable>(tag);
            if (data != null)
                _storage.SetString(key, JsonUtility.ToJson(data));
            else
                _storage.Remove(key);
        }

        public void SaveArray<TSerializable>(TSerializable[] data, string tag = "default")
        {
            string key = _KeyFor<TSerializable>(tag);
            if (data != null)
            {
                var wrapper = new Wrapper<TSerializable> { Items = data };
                _storage.SetString(key, JsonUtility.ToJson(wrapper));
            }
            else _storage.Remove(key);
        }

        public bool Remove<T>(string tag = "default")
        {
            return _storage.Remove(_KeyFor<T>(tag));
        }

        public bool Exist<T>(string tag = "default")
        {
            return _storage.ContainsKey(_KeyFor<T>(tag));
        }

        public IEnumerable<string> EnumerateTags<T>()
        {
            string keyPrefix = _KeyFor<T>("");
            return _storage.EnumerateKeys().Where(key => key.StartsWith(keyPrefix));
        }

        string _KeyFor<T>(string tag) => $"{ typeof(T).Name }_{ tag }";

        [Serializable]
        struct Wrapper<T>
        {
            public T[] Items;
        }
    }
}