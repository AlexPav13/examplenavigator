﻿using UnityEditor;
using UnityEngine;

namespace LimeKit
{
    public static class EditorHelper
    {
        // Player Prefs

#if UNITY_EDITOR_OSX
        [MenuItem("Tools/LimeKit/Show PlayerPrefs", false, 0)]
        public static void ShowPlayerPrefs()
        {
            EditorUtility.OpenWithDefaultApp(string.Format("~/Library/Preferences/unity.{0}.{1}.plist", PlayerSettings.companyName, PlayerSettings.productName));
        }
#endif
        [MenuItem("Tools/LimeKit/Сlean PlayerPrefs", false, 1)]
        public static void CleanPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        // Persistent Datа

        [MenuItem("Tools/LimeKit/Show Persistent Datа Path", false, 20)]
        public static void ShowPersistentDataPath()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }

        [MenuItem("Tools/LimeKit/Сlean Persistent Datа", false, 21)]
        public static void CleanPersistentDatа()
        {
            FileUtil.DeleteFileOrDirectory(Application.persistentDataPath);
        }
    }
}