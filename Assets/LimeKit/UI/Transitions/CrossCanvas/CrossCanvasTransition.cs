using System.Collections;
using LimeKit.Core;
using UnityEngine;
using UnityEngine.EventSystems;



namespace LimeKit.UI
{
    public class CrossCanvasTransition : TransitionAnimator
    {
        public float Duration;
        [SerializeField] AnimationCurve _animationCurve;
        [SerializeField] CanvasGroup _canvasGroup;


        public override TransitionMode GetMode(TransitionAnimation animation)
        {
            return TransitionMode.Serial;
        }

        public override IEnumerator AnimateTransition(TransitionStep step, TransitionContext context)
        {
            //Set parameters
            Duration = (float)context.Animation.dataTransition["duration"] / 2;

            if (context.Animation.Name == "CrossCanvas")
            {
                ShowCanvas();
                if ((step == TransitionStep.ForwardHide || step == TransitionStep.BackwardHide))
                {
                    // Hide
                    Debug.Log("Hide");
                    float time = 0f;
                    while (time <= Duration)
                    {
                        float curveProgress = _animationCurve.Evaluate(time / Duration);
                        Debug.Log(curveProgress);
                        _canvasGroup.alpha = curveProgress;
                        yield return null;
                        time += Time.smoothDeltaTime;
                    }
                    CrossCanvasTransition transition = (CrossCanvasTransition)context.ToAnimator;
                    transition?.ShowCanvas();
                }
                else
                {
                    // Show
                    Debug.Log("Show");
                    _canvasGroup.alpha = 1;
                    CrossCanvasTransition transition = (CrossCanvasTransition)context.FromAnimator;
                    transition?.HideCanvas();
                    float time = 0f;
                    while (time <= Duration)
                    {
                        float curveProgress = _animationCurve.Evaluate(time / Duration);
                        _canvasGroup.alpha = 1 - curveProgress;
                        yield return null;
                        time += Time.smoothDeltaTime;
                    }
                    HideCanvas();
                }
            }
        }

        public void HideCanvas()
        {
            if (_canvasGroup != null)
            {
                _canvasGroup.gameObject.SetActive(false);
            }
        }

        public void ShowCanvas()
        {
            if (_canvasGroup != null)
            {
                _canvasGroup.gameObject.SetActive(true);
            }
        }

        public override bool CanAnimate(TransitionAnimation animation)
        {
            if (animation == null)
            {
                return false;
            }
            else if (animation.Name == "CrossCanvas")
            {
                return true;
            }
            else
                return false;
        }

    }
}