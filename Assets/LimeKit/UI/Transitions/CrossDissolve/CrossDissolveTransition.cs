using System.Collections;
using LimeKit.Core;
using UnityEngine;
using UnityEngine.UI;

namespace LimeKit.UI
{
    public class CrossDissolveTransition : TransitionAnimator
    {
        public float Duration = 0.3f;
        [SerializeField] AnimationCurve _animationCurve;
        [SerializeField] RawImage _image;

        static Texture2D _screenshotTexture;

        public override TransitionMode GetMode(TransitionAnimation animation)
        {
            return TransitionMode.Serial;
        }

        public override IEnumerator AnimateTransition(TransitionStep step, TransitionContext context)
        {
            if (context.Animation == TransitionAnimation.CrossDissolve)
            {
                if (step == TransitionStep.ForwardHide || step == TransitionStep.BackwardHide)
                {
                    // Make screenshot
                    yield return new WaitForEndOfFrame();
                    if (_screenshotTexture == null)
                    {
                        _screenshotTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
                    }
                    _screenshotTexture.Resize(Screen.width, Screen.height);
                    _screenshotTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
                    _screenshotTexture.Apply(false);

                    // Show screenshot
                    _image.texture = _screenshotTexture;
                    _image.color = Color.white;
                    ShowImage();
                    CrossDissolveTransition transition = (CrossDissolveTransition)context.ToAnimator;
                    transition?.ShowImage();
                }
                else
                {
                    if (true)
                    {
                        // Hide screenshot
                        ShowImage();
                        CrossDissolveTransition transition = (CrossDissolveTransition)context.FromAnimator;
                        transition?.HideImage();

                        float time = 0f;
                        while (time <= Duration)
                        {
                            float curveProgress = _animationCurve.Evaluate(time / Duration);
                            _image.color = new Color(1, 1, 1, 1 - curveProgress);
                            yield return null;
                            time += Time.smoothDeltaTime;
                        }
                        HideImage();
                    }
                }
            }
        }

        public void HideImage()
        {
            if (_image != null)
            {
                _image.enabled = false;
            }
        }

        public void ShowImage()
        {
            if (_image != null)
            {
                _image.texture = _screenshotTexture;
                _image.enabled = true;
            }
        }

        public override bool CanAnimate(TransitionAnimation animation)
        {
            if (animation == null)
            {
                return false;
            }
            else if (animation.Name == TransitionAnimation.CrossDissolve.Name)
            {
                return true;
            }
            else
                return false;
        }
    }
}