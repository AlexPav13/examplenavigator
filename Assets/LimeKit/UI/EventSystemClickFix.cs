﻿using LimeKit.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LimeKit.UI
{
    [RequireComponent(typeof(EventSystem))]
    public class EventSystemClickFix : MonoBehaviour
    {
        public int dragThresholdInDP = 5;

        void Start()
        {
            GetComponent<EventSystem>().pixelDragThreshold = ScreenHelper.PixelFromDp(this.dragThresholdInDP);
        }

        void OnValidate()
        {
            Start();
        }
    }
}