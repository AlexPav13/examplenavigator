Shader "LimeKit/ClipTexture" {
    Properties {
        _Radius ("Radius", Float) = 8
        _Width ("Width", Float) = 100
        _Height ("Height", Float) = 100
        _ClipWidth ("Clip Width", Float) = 100
        _ClipHeight ("Clip Height", Float) = 100
        
        [HideInInspector] _MainTex ("Texture", 2D) = "white" {}
        
        // --- Mask support ---
        [HideInInspector] _StencilComp ("Stencil Comparison", Float) = 8
        [HideInInspector] _Stencil ("Stencil ID", Float) = 0
        [HideInInspector] _StencilOp ("Stencil Operation", Float) = 0
        [HideInInspector] _StencilWriteMask ("Stencil Write Mask", Float) = 255
        [HideInInspector] _StencilReadMask ("Stencil Read Mask", Float) = 255
        [HideInInspector] _ColorMask ("Color Mask", Float) = 15
        [HideInInspector] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
        // ---
    }
    SubShader {
        Tags {
            "RenderType"="Transparent"
            "Queue"="Transparent"
        }

        // --- Mask support ---
        Stencil {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZTest [unity_GUIZTestMode]
        ColorMask [_ColorMask]
        // ---
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Radius;
            float _Width;
            float _Height;
            float _ClipWidth;
            float _ClipHeight;

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }

            float AlphaForRoundedCorners(float2 uv, float w, float h, float r){
                float2 newUVInPixels = float2((uv.x - 0.5) * w + .5, (uv.y - 0.5) * h + .5);
                float2 muv = abs(newUVInPixels * 2 - 1) - float2(w, h) + r * 2;
                float d = length(max(0, muv)) / (r * 2);
                return saturate((1 - d) / fwidth(d));
            }

            fixed4 frag (v2f i) : SV_Target {
                // Clip
                float2 clipRatio = float2(_ClipWidth, _ClipHeight) / max(_ClipWidth, _ClipHeight);
                float2 uv = i.uv / clipRatio - (float2(1,1) / clipRatio - float2(1,1)) / 2;

                // Calculate alpha
                float alpha;
                if (min(uv.x, uv.y) < 0 || max(uv.x, uv.y) > 1) {
                    alpha = 0;
                } else {
                    alpha = AlphaForRoundedCorners(uv, _Width * clipRatio.x, _Height * clipRatio.y, _Radius);
                }
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * i.color;
                col.a *= alpha;
                return col;
            }
            ENDCG
        }
    }
}
