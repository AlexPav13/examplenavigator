// Inspired from https://github.com/Nobinator/Unity-UI-Rounded-Corners

using UnityEngine;
using UnityEngine.UI;

namespace LimeKit.UI
{
    [RequireComponent(typeof(Graphic))]
    public class ClipImage : MonoBehaviour
    {
        public float CornerRadius = 10;
        public Vector2 ClipRatio = new Vector2(1, 1);

        [SerializeField] Material _material;

#if UNITY_EDITOR
        void OnValidate()
        {
            if (_material == null)
            {
                _material = new Material(Shader.Find("LimeKit/ClipTexture"));
            }
            GetComponent<Graphic>().material = _material;
            OnRectTransformDimensionsChange();
            Apply();
        }
#endif

        void Start()
        {
            Apply();
        }

        void OnRectTransformDimensionsChange()
        {
            var size = ((RectTransform) transform).rect.size;
            _material?.SetFloat("_Width", size.x);
            _material?.SetFloat("_Height", size.y);
        }

        public void Apply()
        {
            _material?.SetFloat("_Radius", CornerRadius);
            _material?.SetFloat("_ClipWidth", ClipRatio.x);
            _material?.SetFloat("_ClipHeight", ClipRatio.y);
        }
    }

}